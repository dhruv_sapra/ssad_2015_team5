###Date-September 10, '15
##Attendes 
* Saumya Rawat
* Anjali Shenoy
* Dhruv Sapra
* Sreeja Kamishetty
* Ramya K

##Agenda 
Discuss on the Cut scenes and the technology we will be using to develop the game.
###Discussion
* As our client Ms Sandra was out of the country for some prior engagements we had the meeting with some other representative from Polar Bear International which was followed by a brief discussion on Skype with Ms Sandra. The entire discussion was centered around the technology and the graphics that we would be using for the development of the game. Apart from this 2 scenes of the story were also written and discussed with the client.
* Some changes in the story of the game were also suggested by the Polar Bear representative which were discussed in the meet.

###Action Point
* As per the request of the client, the team would be going through the feasibility of using IBM BlueMix and also go through the software Unity.
##Next Meeting
September 16, '15
