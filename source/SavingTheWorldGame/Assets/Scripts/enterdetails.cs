﻿using UnityEngine;
using System.Collections;
using System;
using System.Data;
using Mono.Data.Sqlite;
using System.Collections.Generic;
using UnityEngine.UI;

public class enterdetails : MonoBehaviour {

	private string connectionString;
	public Text InputFullName;
	public Text InputCity;
	public Text InputCountry;
	public Text InputEmail;
	private GUIStyle guiStyle = new GUIStyle();
	public int score;
	private straightmovement obj;

	void Start () {
		connectionString = "URI=file:" + Application.dataPath + "/HighScoreDB.db";
		obj = gameObject.GetComponent<straightmovement>();
		//score = obj.finalscore;
		score = PlayerPrefs.GetInt ("scoree");
	}
	public void EnterName()
	{
		if (InputFullName.text != string.Empty && InputCity.text != string.Empty && InputCountry.text != string.Empty)
		{
			//int score = UnityEngine.Random.Range(1,500);

			InsertScore(InputFullName.text, InputCity.text, InputCountry.text, InputEmail.text, score); 
			InputFullName.text = string.Empty;
			InputCity.text = string.Empty;
			InputCountry.text = string.Empty;
			InputEmail.text = string.Empty;

		}

	}

	private void InsertScore(string name, string city, string country, string email, int newScore)
	{
		using (IDbConnection dbConnection = new SqliteConnection(connectionString))
		{
			dbConnection.Open();
			
			using (IDbCommand dbCmd =dbConnection.CreateCommand())
			{
				string sqlQuery = String.Format("Insert into HighScores(Name, City, Country, Email, Score) values(\"{0}\", \"{1}\", \"{2}\",\"{3}\", \"{4}\")", name, city, country, email, newScore);
				dbCmd.CommandText = sqlQuery;
				dbCmd.ExecuteScalar();
				dbConnection.Close();
			}
			
		}
	}


	void OnGUI() {

		guiStyle.fontSize = 32;
		GUI.Label (new Rect (520, 125, 150, 150), ""+ score.ToString(),guiStyle);
	}

}
