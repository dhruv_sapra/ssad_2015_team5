﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Mono.Data.Sqlite;
using System.Data;
using System;

public class highscoremanager : MonoBehaviour {

	private string connectionString;
	private List<highscore> highScores = new List<highscore>();
	public GameObject scorePrefab;
	public Transform scoreParent;
	public int topRanks;

	// Use this for initialization
	void Start () {
		connectionString = "URI=file:" + Application.dataPath + "/HighScoreDB.db";
		//InsertScore ("ABC", "XYZ", "PQR", "x@y.com", 100);
		//GetScores ();
		ShowScores ();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private void InsertScore(string name, string city, string country, string email, int newScore)
	{
		using (IDbConnection dbConnection = new SqliteConnection(connectionString))
		{
			dbConnection.Open();
			
			using (IDbCommand dbCmd =dbConnection.CreateCommand())
			{
				string sqlQuery = String.Format("Insert into HighScores(Name, City, Country, Email, Score) values(\"{0}\", \"{1}\", \"{2}\",\"{3}\", \"{4}\")", name, city, country, email, newScore);
				dbCmd.CommandText = sqlQuery;
				dbCmd.ExecuteScalar();
				dbConnection.Close();
			}
			
		}

	}

	private void GetScores()
	{
		highScores.Clear ();
		using (IDbConnection dbConnection = new SqliteConnection(connectionString))
		{
			dbConnection.Open();

			using (IDbCommand dbCmd =dbConnection.CreateCommand())
			{
				string sqlQuery = "Select * from HighScores;";
				dbCmd.CommandText = sqlQuery;

				using ( IDataReader reader = dbCmd.ExecuteReader() )
				{
					while(reader.Read())
					{
						Debug.Log(reader.GetString(1));
						highScores.Add(new highscore(reader.GetInt32(5),reader.GetString(1), reader.GetString(2), reader.GetString(3),reader.GetString(4), reader.GetInt32(0)));
					}
					dbConnection.Close();
					reader.Close();
				}
			}
		}
		highScores.Sort ();

	}

	private void ShowScores()
	{
		GetScores ();
		//for (int i = 0; i < highScores.Count; i++) prints all values in DB
		for (int i = 0; i < topRanks; i++) 
		{
			if ( i <= highScores.Count - 1)
			{
			GameObject tmpObjec = Instantiate(scorePrefab);
			highscore tmpScore = highScores[i];

			tmpObjec.GetComponent<highscorescript>().SetScore(tmpScore.name, tmpScore.score.ToString(), "#"+(i+1).ToString());
			tmpObjec.transform.SetParent(scoreParent);
			//to scale objects;
		   	tmpObjec.GetComponent<RectTransform>().localScale = new Vector3 (1,10,1);	
			}
		}

	}
}
