﻿using UnityEngine;
using System.Collections;

public class TouchMovement : MonoBehaviour
{
	
	public float speed;
	public int score;
	//private GUIStyle guiStyle = new GUIStyle();
	public Font MyFont;
	//public float x;
	// Use this for initialization
	void Start ()
	{
		speed = 0.5f;
		score = 0;
		//	x = 10f;
	}
	
	// Update is called once per frame
	void Update ()
	{
	  //  float moveH = Input.GetAxis("Horizontal");
		//float moveV = Input.GetAxis("Vertical");
		float moveH = Input.GetAxis("Mouse X");
		
		float moveV = Input.GetAxis("Mouse Y");
		
		if (Input.touchCount > 0)
		{
			moveH = Input.touches[0].deltaPosition.x;
			moveV = Input.touches[0].deltaPosition.y;
		}
		//float moveV = 1;
		// transform.Translate(moveH * speed * Time.deltaTime, 0f, moveV * speed * Time.deltaTime);
		//if (moveH == 1)
		//{
		//transform.rotation = new Quaternion(0, 90, 0, 0);
		//  moveH = -1;
		//}
		// if (moveV == 1)
		//{
		// transform.rotation = new Quaternion(0, 0, 0, 0);
		//transform.rotation = new Vector3 (0,90,0)
		// transform.Rotate(0, 90, 0);
		// moveH = -1;
		//}
		transform.Rotate(0, moveH * 8 * Time.deltaTime, 0);
		if(moveV==0)
			transform.Translate(-1 * speed * Time.deltaTime, 0f, 0f);
		else
			transform.Translate(-1 * moveV * 1.5f * speed * Time.deltaTime, 0f, 0f);
		//transform.Translate(1f * x, 0f, 0f);
		//x = x + 1f;
		
		//moveH* speed *Time.deltaTime
		//moveV * speed * Time.deltaTime
	}
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag ("BIO"))
		{

			other.gameObject.SetActive(false);
			score = score + 10;
			//AudioSource.PlayClipAtPoint(clip, transform.position);
		}
		if(other.gameObject.CompareTag("PLASTIC"))
		{
			other.gameObject.SetActive(false);
			score = score - 5;
			//AudioSource.PlayClipAtPoint(clip, transform.position);
		}
		PlayerPrefs.SetInt("scoree",score);
	}
	void OnGUI() {
	
		GUI.skin.font = MyFont;
		GUI.Box ( new Rect (  ((Screen.width)/2 + (Screen.width)/4), ((Screen.height)/9-(Screen.height)/8), (Screen.width)/4,(Screen.height)/4 ),"Score: "+"<size=32>"+score.ToString ("F1")+"</size>" );  
	
	}

	public int getScore()
	{
		return score;
	}
}



/*var Speed        : float = 0;//Don't touch this
var MaxSpeed     : float = 10;//This is the maximum speed that the object will achieve
var Acceleration : float = 10;//How fast will object reach a maximum speed
var Deceleration : float = 10;//How fast will object reach a speed of 0

function Update () {
	if ((Input.GetKey ("left"))&&(Speed < MaxSpeed))
		Speed = Speed - Acceleration  Time.deltaTime;
	else if ((Input.GetKey ("right"))&&(Speed > -MaxSpeed))
		Speed = Speed + Acceleration  Time.deltaTime;
	else
	{
		if(Speed > Deceleration  Time.deltaTime)
			Speed = Speed - Deceleration  Time.deltaTime;
		else if(Speed < -Deceleration  Time.deltaTime)
			Speed = Speed + Deceleration  Time.deltaTime;
		else
			Speed = 0;
	}
	
	transform.position.x = transform.position.x + Speed * Time.deltaTime;
}*/

